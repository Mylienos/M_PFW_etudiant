{ pkgs ? import <nixpkgs> {} }:

let 
  _musicwspg = pkgs.haskellPackages.callCabal2nix "musicwspg" ./. {};

  _drv = pkgs.stdenv.mkDerivation {
    name = "musicwspg";
    src = ./.;
    buildInputs = [ _musicwspg ];
    installPhase = ''
      mkdir $out
      cp data/* $out/
      cp ${_musicwspg}/bin/musicwspg $out/
    '';
  };

in
if pkgs.lib.inNixShell then _musicwspg.env else _drv

