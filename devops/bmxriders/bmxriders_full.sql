DROP DATABASE IF EXISTS bmxridersdb;
CREATE DATABASE bmxridersdb;

\connect bmxridersdb

\ir bmxriders.sql

DROP ROLE IF EXISTS bmxridersuser;
CREATE ROLE bmxridersuser WITH LOGIN PASSWORD 'toto';
GRANT SELECT ON TABLE bmxrider TO bmxridersuser;

