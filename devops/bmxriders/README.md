
# run locally

- install cabal and postgresql

```
nix-shell
psql -U postgres -f bmxriders_full.sql
export DATABASE_URL="host=localhost port=5432 dbname=bmxridersdb user=bmxridersuser password='toto'"
export PORT=8000
firefox "localhost:8000" &
cabal run bmxriders-server
```


# deploy with nixops

- install nixops

```
nixops create -d vm1 nixops.nix
time nixops deploy -d vm1 --force-reboot
firefox "192.168.56.101" &
```

- monitoring

```
nixops ssh -d vm1
systemctl status bmxriders
```


# deploy to heroku

- install heroku-cli and postgresql

```
heroku login
heroku create --buildpack https://github.com/mfine/heroku-buildpack-stack.git
heroku addons:create heroku-postgresql:hobby-dev
heroku psql -f bmxriders.sql
time git push heroku master
heroku open
```

