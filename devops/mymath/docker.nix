# { pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.09.tar.gz") {} }:
{ pkgs ? import (<nixpkgs>) {} }:

let

  myapp = import ./default.nix { inherit pkgs; };

  entrypoint = pkgs.writeScript "entrypoint.sh" ''
    #!${pkgs.stdenv.shell}
    $@
  '';

in

  pkgs.dockerTools.buildImage {
    name = "mymath-server";
    config = {
      Entrypoint = [ entrypoint ];
      Cmd = [ "${myapp}/bin/mymath-server" ];
    };
  }

#    --docroot . --http-address 0.0.0.0 --http-port $PORT