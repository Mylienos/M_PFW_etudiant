{
  network.description = "mymath";
  webserver = { config, pkgs, ... }:
    let 
        mymath = import ./default.nix { inherit pkgs; };
    in
    {
        # spécification logique
        networking.firewall.allowedTCPPorts = [ 80 ];
        systemd.services.mymath = {
            wantedBy = ["multi-user.target"];
            after = [ "network.tareget"];
            script = "PORT=80 ${mymath}/bin/mymath-server";
        };

        # spécification physique
        deployment = {
        targetEnv = "virtualbox";
        virtualbox = {
            memorySize = 512;
            vcpu = 1;
            headless = true;
        };
    };
  };
}