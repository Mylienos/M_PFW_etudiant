{ pkgs ? import <nixpkgs> {} }:
let
  drv = pkgs.haskellPackages.callCabal2nix "timezone-client" ./. {};
in
if pkgs.lib.inNixShell then drv.env else drv

