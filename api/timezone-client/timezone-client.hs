{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Data.Aeson (FromJSON, decode)
import Data.ByteString.Lazy (ByteString)
import Data.Text.Lazy (Text)
import GHC.Generics (Generic)
import Network.Connection (TLSSettings (..))
import Network.HTTP.Conduit (httpLbs, mkManagerSettings, newManager, parseRequest, responseBody)
import System.Environment (getArgs)

tlsQuery :: String -> IO ByteString
tlsQuery url = do
    manager <- newManager $ mkManagerSettings (TLSSettingsSimple True False False) Nothing
    request <- parseRequest url
    httpLbs request manager >>= return . responseBody

main :: IO ()
main = do
    putStrLn "TODO"

