{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

-- import           Data.Aeson (FromJSON, decodeStrict)
-- import           Data.Maybe (fromMaybe)
import qualified Data.Text as T
-- import           GHC.Generics (Generic)
-- import qualified JavaScript.Web.XMLHttpRequest as XHR
import           Miso
import qualified Miso.String as MS

data Model = Model 
    { modelText :: T.Text
    } deriving (Eq, Show)

data Action 
    = SetModelText MS.MisoString 
    | NoOp 
    deriving (Show, Eq)

main :: IO ()
main = startApp App 
    { model = Model ""
    , update = updateModel
    , view = viewModel
    , subs = []
    , events = defaultEvents
    , initialAction = NoOp
    , mountPoint = Nothing
    }

updateModel :: Action -> Model -> Effect Action Model
updateModel (SetModelText str) m = noEff m { modelText = MS.fromMisoString str }
updateModel NoOp m = noEff m

viewModel :: Model -> View Action
viewModel (Model mt) = 
    span_ []
        [ h1_ [] [ text "Bmx riders (Miso)" ]
        , p_ [] [ input_ [ onInput SetModelText ] ]
        , p_ [] [ text $ MS.toMisoString mt ]
        ]

