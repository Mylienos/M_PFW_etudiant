{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Data.Text.Lazy as L
import qualified Clay as C
import           Lucid
import           Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)
import qualified Database.PostgreSQL.Simple as SQL
import           System.Environment (getArgs, getProgName)

data Category = Category 
        { _cid :: Int,
          _cname :: T.Text,
          _cimage :: T.Text
        } deriving Show

instance FromRow Category where
    fromRow = Category <$> field <*> field <*> field

data Product = Product 
        { _pid :: Int,
          _pcategory :: Int,
          _pname :: T.Text,
          _pprice :: Double,
          _pimage :: T.Text
        } deriving Show

instance FromRow Product where
    fromRow = Product <$> field <*> field <*> field <*> field <*> field

getCategory :: SQL.Connection -> IO [Category]
getCategory conn = SQL.query_ conn "SELECT * FROM category" :: IO [Category]

getProduct :: SQL.Connection -> IO [Product]
getProduct conn = SQL.query_ conn "SELECT * FROM product" :: IO [Product]

myCss :: C.Css
myCss = do
  C.ul C.? do
    C.backgroundColor  C.beige
    C.border           C.solid (C.px 1) C.black
  C.body C.? do
    C.backgroundColor C.yellow

makeCategoriesPage :: [Category] -> Html ()
makeCategoriesPage categories =  do
  doctype_
  html_ $ do
      head_ $ do
          meta_ [charset_ "utf-8"]
          style_ $ L.toStrict $ C.render myCss
      body_ $ do
          h1_ "Megalozon.com"
          ul_ $ mapM_ ( li_ . toHtml . _cname) categories

main :: IO ()
main = do
    args <- getArgs
    if length args /= 1
    then do
        progName <- getProgName
        putStrLn $ "usage: " ++ progName ++ " <output html>"
    else do
        let [outFile] = args

        conn <- SQL.connectPostgreSQL "host='localhost' port=5432 dbname=megalozon user=megaluser password='megaluser'"
        categories <- getCategory conn
        products <- getProduct conn
        SQL.close conn

        mapM_ print products


        