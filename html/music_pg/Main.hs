{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)
import qualified Database.PostgreSQL.Simple as SQL

data Name = Name T.Text deriving Show

instance FromRow Name where
    fromRow = Name <$> field 

main :: IO ()
main = do
    conn <- SQL.connectPostgreSQL "host='localhost' port=5432 dbname=music_pg user=toto password='toto'"

    putStrLn "\n*** id et nom des artistes ***"
    res <- SQL.query_ conn
        "SELECT * FROM artists"
        :: IO [(Int, T.Text)]
    mapM_ print res

    putStrLn "\n*** id et nom de l'artiste 'Radiohead' ***"
    res2 <- SQL.query_ conn
        "SELECT * FROM artists WHERE name = 'Radiohead'"
        :: IO [(Int, T.Text)]
    mapM_ print res2

    putStrLn "\n*** tous les champs des titres dont le nom contient 'ust' et dont l'id > 1 ***"
    res3 <- SQL.query_ conn
        "SELECT * FROM titles WHERE name LIKE '%ust%' AND id > 1"
        :: IO [(Int, Int, T.Text)]
    mapM_ print res3

    putStrLn "\n*** noms des titres (en utilisant le type Name) ***"
    res4 <- SQL.query_ conn
        "SELECT name FROM titles"
        :: IO [Name]
    mapM_ print res4

    putStrLn "\n*** noms des titres (en utilisant une liste de T.Text) ***"
    res5 <- SQL.query_ conn
        "SELECT name FROM titles"
        :: IO [[T.Text]]
    mapM_ print res5

    SQL.close conn

