{ pkgs ? import <nixpkgs> {} }:
let
  drv = pkgs.haskellPackages.callCabal2nix "music_pg" ./. {};
in
if pkgs.lib.inNixShell then drv.env else drv

