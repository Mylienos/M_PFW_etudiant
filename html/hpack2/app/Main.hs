{-# LANGUAGE OverloadedStrings #-}

import qualified Clay as C
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Data.Text.Lazy as L
import           Lucid
import           System.Environment (getArgs, getProgName)

myCss :: C.Css
myCss = C.ul C.? do
  C.backgroundColor  C.beige
  C.border           C.solid (C.px 1) C.black

myPage :: T.Text -> Html ()
myPage file = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            style_ $ L.toStrict $ C.render myCss
        body_ $ do
            h1_ "Content"
            ul_ $ mapM_ ( li_ . toHtml ) (T.lines file)

main :: IO ()
main = do
    args <- getArgs
    if length args /= 2
    then do
        progName <- getProgName
        putStrLn $ "usage: " ++ progName ++ " <input dat> <output html>"
    else do
        let [inFile, outFile] = args
        file <- TIO.readFile inFile
        renderToFile outFile $ myPage file