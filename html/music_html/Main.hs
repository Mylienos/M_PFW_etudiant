{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Data.Text.Lazy as L
import qualified Clay as C
import           Lucid
import           Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)
import qualified Database.PostgreSQL.Simple as SQL
import           System.Environment (getArgs, getProgName)

data Music = Music 
        { _title :: T.Text,
         _artist :: T.Text 
        } deriving Show

instance FromRow Music where
    fromRow = Music <$> field <*> field

getTiltesArtists :: SQL.Connection -> IO [Music]
getTiltesArtists conn = SQL.query_ conn "SELECT t.name, a.name FROM titles AS t INNER JOIN artists AS a ON t.artist = a.id" :: IO [Music]

myCss :: C.Css
myCss = C.ul C.? do
  C.backgroundColor  C.beige
  C.border           C.solid (C.px 1) C.black

myPage :: [Music] -> Html ()
myPage musics = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            style_ $ L.toStrict $ C.render myCss
        body_ $ do
            h1_ "Content"
            ul_ $ mapM_ ( li_ . toHtml . formatMusic) musics

formatMusic :: Music -> T.Text
formatMusic m = T.concat([_title m, " - ", _artist m])

main :: IO ()
main = do
    args <- getArgs
    if length args /= 1
    then do
        progName <- getProgName
        putStrLn $ "usage: " ++ progName ++ " <output html>"
    else do
        let [outFile] = args

        conn <- SQL.connectPostgreSQL "host='localhost' port=5432 dbname=music_pg user=toto password='toto'"
        musics <- getTiltesArtists conn
        SQL.close conn

        renderToFile outFile $ myPage musics