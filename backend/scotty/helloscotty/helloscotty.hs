{-# LANGUAGE OverloadedStrings #-}
import qualified Clay as C
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import           Lucid
import qualified Web.Scotty as S

divCss =
    C.div C.# C.byClass "divCss" C.? do
        C.backgroundColor  C.beige
        C.border           C.solid (C.px 1) C.black



main :: IO ()
main = S.scotty 3001 $ do
    S.get "/" $ S.html $ renderText 
        $ html_ $ do
            head_ $ 
                style_ $ L.toStrict $ C.render divCss
            body_ $ do
                h1_ "The helloscotty project"
                a_ [ href_ "hello" ] "go to hello page"
    S.get "/hello" $ do
        name <- S.param "name" `S.rescue` (\_ -> return (""::T.Text))
        S.html $ renderText $ 
            html_ $ do
                head_ $ 
                    style_ $ L.toStrict $ C.render divCss
                body_ $ do
                    h1_ $ toHtml $ T.concat ["Hello ", name, " !"]
                    div_ $
                        form_ [action_ "/hello", method_ "get"] $ do
                            input_ [placeholder_ "Your cute name", name_ "name"]
                            input_ [type_ "submit", value_ "Valid"]
                    a_ [ href_ "/" ] "go to home page"
