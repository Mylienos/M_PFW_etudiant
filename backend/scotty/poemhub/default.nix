{ pkgs ? import <nixpkgs> {} }:
let
  drv = pkgs.haskellPackages.callCabal2nix "poemhub" ./. {};
in
if pkgs.lib.inNixShell then drv.env else drv

