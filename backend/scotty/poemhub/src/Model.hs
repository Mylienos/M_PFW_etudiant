{-# LANGUAGE OverloadedStrings #-}

module Model where

import qualified Data.Text as T
import qualified Database.SQLite.Simple as SQL
import           Database.SQLite.Simple.FromRow (FromRow, fromRow, field)

dbName = "poemhub.db"

data Poem = Poem 
  { id :: Int
  , author :: T.Text
  , title :: T.Text
  , year :: Int
  , body :: T.Text 
  }

instance FromRow Poem where
  fromRow = Poem <$> field <*> field <*> field <*> field <*> field

getAllPoems :: IO [Poem]
getAllPoems = do
  conn <- SQL.open dbName
  peoms <- SQL.query_ conn "SELECT * FROM poems" :: IO [Poem]
  SQL.close conn
  return peoms
  
getPoemById :: Int -> IO Poem
getPoemById idd = do
  conn <- SQL.open dbName
  poem <- SQL.query conn "SELECT * FROM poems WHERE id=?" (SQL.Only idd) :: IO [Poem]
  SQL.close conn
  return (head poem)

getYearText :: Poem -> T.Text
getYearText = T.pack . show . Model.year

getIdText :: Poem -> T.Text
getIdText = T.pack . show . Model.id

addPoem :: String -> String -> String -> String -> Bool
addPoem auth title year body = True

