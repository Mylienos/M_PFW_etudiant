{-# LANGUAGE OverloadedStrings #-}

module View (mkpage, homeRoute, poemRoute, writePoemRoute) where

import qualified Clay as C
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import           Lucid

import qualified Model

myCss :: C.Css
myCss = do
    C.div C.# C.byClass "divCss" C.? do
        C.backgroundColor  C.beige
        C.border           C.solid (C.px 1) C.black
        C.margin           (C.em 1) (C.em 1) (C.em 1) (C.em 1)
        C.padding          (C.em 0) (C.em 1) (C.em 0) (C.em 1)
        C.width            (C.px 320)
        C.float            C.floatLeft
    C.div C.# C.byClass "divPoem" C.? do
      C.border           C.solid (C.px 1) C.black
      C.width            (C.px 320)
    C.p C.# C.byClass "pCss" C.?
        C.fontWeight C.bold

mkpage :: Lucid.Html () -> Lucid.Html () -> L.Text
mkpage titleStr page = renderText $ html_ $ do
  head_ $ do
    style_ $ L.toStrict $ C.render myCss
    title_ titleStr
  body_ page

homePoem :: Model.Poem -> Lucid.Html ()
homePoem poem = div_ [class_ "divCss"] $
                  a_ [class_ "aCss", href_ (T.concat ["/read?id=", Model.getIdText poem])] $ do
                    h3_ (toHtml (Model.title poem))
                    p_  (toHtml (T.concat ["by ", Model.title poem, " (", Model.getYearText poem,")"]))

homeRoute :: [Model.Poem] -> Lucid.Html ()
homeRoute poems = do
  h1_ "Poem hub"
  a_ [href_ "/write"] "Write a poem"
  ul_ $ mapM_ homePoem poems

poemRoute :: Model.Poem -> Lucid.Html ()
poemRoute poem = do
  p_ [class_ "pCss"] (toHtml (Model.title poem))
  p_ (toHtml (T.concat ["by ", Model.author poem, " (", Model.getYearText poem,")"]))
  div_ [class_ "divPoem"] (toHtml (Model.body poem))
  a_ [href_ "/"] "Back Home"

writePoemRoute :: Lucid.Html ()
writePoemRoute = do
  a_ [href_ "/"] "Back Home"
  form_ [action_ "/write", method_ "POST"] $ do
    div_ $ do
      label_ "Author"
      input_ [placeholder_ "Author", name_ "author"]
    div_ $ do
      label_ "Title"
      input_ [placeholder_ "Title", name_ "title"]
    div_ $ do
      label_ "Year"
      input_ [placeholder_ "Year", name_ "year"]
    div_ $ do
      label_ "Body"
      input_ [placeholder_ "Body", name_ "body"]
    input_ [type_ "submit", value_ "Valid"]
    


