{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.Trans (liftIO)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Web.Scotty (get, middleware, param, post, rescue, scotty, html, redirect)
import qualified Data.Text.Lazy as L

import System.Random (getStdGen)

import qualified Model
import qualified View

-- TODO implement routes

main = scotty 3001 $ do
        middleware logStdoutDev

        get "/" $ do
          poems <- liftIO Model.getAllPoems
          html $ View.mkpage "Poem hub - Home" $ View.homeRoute poems

        get "/read" $ do
          id <- param "id" `rescue` (\_ -> return (0::Int))
          poem <- liftIO (Model.getPoemById id)
          html $ View.mkpage "Poem hub - Read poem" $ View.poemRoute poem
        
        get "/write" $
          html $ View.mkpage "Poem hub - Write poem" $ View.writePoemRoute
        
        post "/write" $ do
          title <- param "title"
          author <- param "author"
          body <- param "body"
          year <- param "year"
          Model.addPoem author title year body
          redirect "/"

